import * as React from "react";
import "./App.css";

import { XkcdImage } from "./XkcdImage";

class App extends React.Component {
  public render() {
    return <XkcdImage />;
  }
}

export default App;

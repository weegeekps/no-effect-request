import React, { useState } from "react";
import { retrieveXkcdComic } from "./XkcdApi";

export function XkcdImage() {
  const [comicSrc, setComicSrc] = useState("");
  const [comicAlt, setComicAlt] = useState("");
  const [initialLoad, setInitialLoad] = useState(true);

  const updateComic = async (n?: number) => {
    const comic = await retrieveXkcdComic(n);

    setComicAlt(comic.alt || "");
    setComicSrc(comic.img);
  };

  const handleUpdateClick = (e: React.MouseEvent) => {
    updateComic();
  };

  if (initialLoad) {
    setInitialLoad(false);
    updateComic();
  }

  return (
    <>
      {comicSrc && <img src={comicSrc} alt={comicAlt} />}
      <button onClick={handleUpdateClick}>Update</button>
    </>
  );
}

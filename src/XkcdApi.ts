const XKCD_URL_BASE = `https://xkcd.now.sh`; // CORS enabled version of the XKCD api https://github.com/mrmartineau/xkcd-api

export interface IXkcdComic {
  month?: string;
  num?: number;
  link?: string;
  year?: string;
  news?: string;
  safe_title?: string;
  transcript?: string;
  alt?: string;
  img: string;
  title?: string;
  day?: string;
}

export async function retrieveXkcdComic(n?: number): Promise<IXkcdComic> {
  const url = n ? `${XKCD_URL_BASE}/${n}` : `${XKCD_URL_BASE}`;

  // tslint:disable-next-line:no-console
  console.log("Making request.");

  return new Promise((resolve, reject) => {
    fetch(url)
      .then(response => response.json())
      .then(payload => resolve(payload))
      .catch(e => {
        // tslint:disable-next-line:no-console
        console.error(e);
        reject(e);
      });
  });
}
